mvn clean install

mv target/demo-ms-user-api-1.0-SNAPSHOT.jar api-image/demo-ms-user-api-1.0-SNAPSHOT.jar

cd api-image

docker build -t user-api-demo .

cd ../minio-image

docker build -t user-minio-demo .