\documentclass{beamer}
\usetheme[faculty=econ]{fibeamer}

\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage[T1]{fontenc}
\usepackage{xcolor}

\lstset{
  language=Java,                
  basicstyle=\scriptsize,
  escapeinside={*@}{*@},
  frame=single,
  xleftmargin=2mm,
  xrightmargin=2mm,
  keepspaces=true,
  tabsize=2
}

\newcounter{ctr1}
\title[AW]{\Large{Advanced Web}}
\author[C. Tibermacine]{\large{Chouki~Tibermacine}\\
\small{Chouki.Tibermacine@umontpellier.fr}}
%\institute{Polytech Montpellier}
\date{\tiny{DO4 2023-2024}}

\begin{document}
	
	\begin{frame}
		\titlepage
		\begin{flushright}
			\includegraphics[width=3.5cm]{figs/polytech.png}
		\end{flushright}
	\end{frame}
	

\AtBeginSection[]{% Print an outline at the beginning of sections
  \begin{frame}<beamer>
    \frametitle{Plan du cours}
    % \frametitle{Outline}
    \tableofcontents[currentsection]
    % \tableofcontents
  \end{frame}}

\AtBeginSubsection[]{% Print an outline at the beginning of sections
  \begin{frame}<beamer>
    \frametitle{Plan du cours}
    % \frametitle{Outline}
    \tableofcontents[currentsubsection]
    % \tableofcontents
  \end{frame}}

\section{Introduction}

\begin{frame}
	\frametitle{AuthN vs AuthZ}
	\begin{itemize}
		\item Authentification (AuthN) : processus de vérification de l'identité d'un utilisateur 
		\begin{itemize}
			\item Qui êtes vous ?
		\end{itemize}
		\item Autorisation (AuthZ) : processus de vérification si l'utilisateur a le droit d'accès nécessaire pour une ressource protégée (page Web, endpoint d'API REST, ...)
		\begin{itemize}
			\item Pourriez-vous faire cela ?
		\end{itemize}
	\end{itemize}
\end{frame}

\section{Framework d'autorisation OAuth2}

\begin{frame}
	\frametitle{Pourquoi OAuth2 ?}
	Supposons le scénario suivant :
		\begin{itemize}
			\item Vous êtes ingénieur au CERN à Genève
			\item Lors de votre prise de fonctions, l'accueil du CERN vous a remis un badge (une carte RFID) qui vous permet d'accéder à votre bureau, aux salles de réunion et à certains datacenters
			\item Un matin, alors que vous vous apprêtez à aller en salle de réunion, un jeune stagiaire, qui collabore avec vous, vous croise et aimerait récupérer une clé USB que vous avez laissée dans votre bureau
		\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Pourquoi OAuth2 ? -suite-}	
		\begin{itemize}
			\item Vous n'avez pas forcément envie de lui donner votre badge
			\item Pourquoi ? Je vous laisse deviner
			\item Vous lui demandez alors de voir avec l'accueil pour avoir l'autorisation d'accès
			\item Le jeune stagiaire passe à l'accueil et leur expose sa requête en leur montrant son badge
			\item L'accueil vous appelle pour vous authentifier (par reconnaissance vocale) et vous expose la demande
			\item Vous confirmez votre accord à l'accueil
			\item L'accueil remet alors un badge à usage unique ou utilisable pendant une courte durée pour que le stagiaire puisse accéder au bureau uniquement et récupérer la clé USB
			
		\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Pourquoi OAuth2 ? -suite-}
	\begin{itemize}
		\item A travers cet exemple, nous avons implémenter un processus simplifié d'autorisation OAuth2
		
		\item Objectif : ne pas divulguer ses secrets (son username/password, ou son badge) à un tiers pour accéder à une ressource protégée dont il n'est pas le serveur
		
		\item Quelles entités ?
		\begin{itemize}
			\item Protected Resource (PR) : bureau
			\item Resource Owner (RO) : vous
			\item Resource Server (RS) : bâtiment du CERN
			\item Authorization Server (AS) : accueil du CERN
			\item Client : le jeune stagiaire
		\end{itemize}
		
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Qu'est-ce que OAuth2 ? {\footnotesize(depuis \url{auth0.com})}}
	\begin{itemize}
		\item OAuth signifie \textit{Open Authorization}
		\item C'est une norme qui permet à une application d'accéder à une ressource protégée hébergée par une autre application au nom d'un utilisateur
		\item OAuth 2 a remplacé OAuth 1 en 2012 et est devenu le standard industriel pour l'autorisation en ligne
		\item OAuth 2 :		
		\begin{itemize} 
			\item permet un accès consenti,
			\item limite les actions que l'application cliente peut réaliser sur les ressources au nom de l'utilisateur,
			\item sans jamais partager les informations d'identification de l'utilisateur.
		\end{itemize}
		\item OAuth 2 est un protocole d'autorisation et NON un protocole d'authentification
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Principes de fonctionnement}
	\begin{itemize}
		\item OAuth 2.0 utilise des \textbf{jetons d'accès} (\textit{access tokens})
		\item Un jeton d'accès est une donnée qui représente l'autorisation d'accéder à une ressource et réaliser une certaine action dessus au nom de l'utilisateur final
		\item OAuth 2 ne définit pas de format spécifique pour les jetons d'accès. En pratique, c'est le format JSON Web Token (JWT) qui est souvent utilisé
		\item Pourquoi ? cela permet aux émetteurs de jetons d'inclure des données dans le jeton lui-même
		\item Exemple de donnée incluse : une date d'expiration pour renforcer la sécurité
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{En pratique, on a affaire à :}	
	\begin{itemize}
		\item Protected Resource (PR) : un API endpoint protégé,
		\item Resource Owner (RO) : l'utilisateur ou le système qui possède les ressources protégées et peut en accorder l'accès,
		\item Resource Server (RS) : fournit l'API et reçoit des demandes d'accès. Il accepte et valide un jeton d'accès du client et lui renvoie les ressources appropriées,
		\item Authorization Server (AS) : reçoit les demandes de jetons d'accès de la part du client et les délivre après authentification et consentement du RO. Il expose 2 endpoints :
		\begin{enumerate}
			\item authorization endpoint: gère l'authentification et le consentement interactifs de l'utilisateur
			\item token endpoint: fait partie d'une interaction machine-machine
		\end{enumerate}
		\item Client : le système qui a besoin d'accéder aux ressources protégées. Pour accéder aux ressources, le client doit détenir le jeton d'accès approprié
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Scope (champ d'application)}
	\begin{itemize}
		\item Il permet de spécifier exactement le motif pour lequel l'accès aux ressources peut être accordé
		
		\item Un client peut requérir 1 ou plusieurs scopes
		\item Cette information est présentée à l'utilisateur dans l'écran de consentement
		\item Le jeton d'accès délivré au client sera limité aux scopes consentis
		
		\item Les valeurs de scope acceptables, et les ressources auxquelles elles se rapportent, dépendent du serveur de ressources
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Codes d'autorisation avant jetons d'accès}
	\begin{itemize}
		\item Le serveur d'autorisation OAuth 2 ne peut pas renvoyer directement un jeton d'accès après que le propriétaire de la ressource ait consenti l'accès
		\item À la place, et pour une meilleure sécurité, il envoie un \textbf{code d'autorisation} temporaire qui est ensuite échangé contre un \textbf{jeton d'accès}
		\item Pourquoi ? je vous laisse réfléchir quelques instants
		\item Reprenons notre exemple où vous êtes ingénieur au CERN, et supposons que:		
		\begin{itemize}
			\item le stagiaire ne s'est pas déplacé vers vous mais vous a appelé
			\item vous le redirigez depuis votre téléphone vers celui de l'accueil
			\item devant les portes des bureaux il y des boitiers pour saisir un code qui déverrouille la porte. Le stagiaire demande alors à l'accueil un code d'accès au lieu de récupérer un badge
		\end{itemize} 
	\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Requête de demande de jeton d'accès}
	\begin{itemize}		
		\item Le canal utilisé pour demander l'autorisation après consentement (redirection d'appel, dans l'exemple) n'est pas jugé(e) digne de confiance
		\item Une nouvelle requête doit être transmise par le client vers le serveur d'autorisation (un nouvel appel effectué par le stagiaire à l'accueil, qui pourra alors l'authentifier en voyant son numéro)
		\item Grâce au code d'autorisation envoyé dans cette nouvelle requête une authentification peut être mise en place par le serveur d'autorisation
		\item Le client envoie dans cette requête son \texttt{client\_id} (l'id avec lequel il s'est enregistré auprès du serveur d'autorisation) et le \texttt{client\_secret} (un code secret comme une clé d'API par ex)
	\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Jetons d'actualisation}
\begin{itemize}		
		\item Le serveur d'autorisation peut également émettre un \textbf{jeton d'actualisation} (\textit{refresh token}) avec le jeton d'accès
		\item Contrairement aux jetons d'accès, les jetons d'actualisation ont une longue durée d'expiration et peuvent être échangés contre de nouveaux jetons d'accès lorsque ces derniers expirent
		\item Dans de nombreux cas, on utilise une durée de validité d'env. 30 minutes à plusieurs jours pour les refresh tokens contre env. 5 minutes pour les access tokens
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Types d'attributions (Grants) OAuth 2}
	Les attributions sont l'ensemble des étapes (flux) que doit suivre un client pour avoir l'autorisation d'accès à la ressource
	\begin{itemize}
		\item \textit{Implicit Grant}: un flux simplifié où le serveur d'autorisation renvoie directement le jeton d'accès. Problème de fuite de jeton
		\item \textit{Authorization Code Grant}: le flux avec envoi de code d'autorisation à usage unique, échangé contre un jeton d'accès. 
		\begin{itemize}
			\item Fonctionne bien dans beaucoup de cas
			\item Mais dans les SPA ou front mobile, le code secret du client ne peut pas être stocké de façon sécurisée
		\end{itemize}
		
	\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Types d'attributions (Grants) OAuth 2 -- suite}
	\begin{itemize}
		\item \textit{Authorization Code Grant with Proof Key for Code Exchange (PKCE)}: flux plus sécurisé, grâce aux étapes supplémentaires qui précèdent l'obtention d'un jeton d'accès (voir plus loin)
		\item \textit{Resource Owner Credentials Grant Type}: le client doit d'abord obtenir les credentials du RO. N'est utilisable que lorsque il y a confiance totale au client (et où l'indirection est impossible)
		\item \textit{Client Credentials Grant Type}: utilisable dans les applications non-interactices, entre microservices par ex (id du client et code secret utilisés directement dans les requêtes)
		\item \textit{Refresh Token Grant}
	\end{itemize}
	Autres types d'attributions, certains sont standardisés comme extensions OAuth 2 comme :
	\begin{itemize}
		
		\item \textit{Device Authorization Flow}: utilisable dans les apps sur des devices contraints par les entrées (smart TV)
		
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Authorization Code Flow with PKCE}
	\begin{itemize}
		\item C'est une extension au flux ``\textit{authorization code flow}''
		\item Elle était conçue à la base pour être utilisée avec les applications mobiles ou SPA, mais est désormais recommandée pour les autres types d'applications
		\item Elle permet de prévenir les attaques de type CSRF et les attaques par injection de code d'autorisation
		\item Le client doit créer un autre code secret pour chaque requête d'autorisation et utiliser ce code pour échanger le code d'autorisation contre un jeton d'accès
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Authorization Code Flow with PKCE : requête d'autorisation}
	\begin{itemize}
		\item A chaque requête d'autorisation, le client génère :
		\begin{enumerate}
			\item un code secret nommé ``code verifier'' : chaîne de caractères aléatoire entre 43 et 128 chars
			\item un ``code challenge'' : hachage SHA256 du ``code verifier'' sous la forme d'une chaîne base64-URL-encoded  (et si ce n'est pas possible de hacher, envoyer le code en clair (\textit{plain}))
		\end{enumerate}
		\item Le client envoie dans la 1ère requête d'authZ le ``code challenge'' et la méthode de hachage (SHA256 ou plain)
		\item Le serveur d'autorisation récupère ces données et les stocke avec le code d'autorisation retourné
		\item Il répond en envoyant uniquement le code d'autorisation
		\item En cas d'absence de code challenge, le serveur d'autorisation doit retourner un \texttt{error=invalid\_request} et un \texttt{error\_description} ou \texttt{error\_uri} qui décrit l'erreur
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Authorization Code Flow with PKCE : échange de code d'autorisation}
	\begin{itemize}
		\item Dans la 2nde requête dans laquelle le client envoie le code d'autorisation, il envoie également le ``code verifier''
		\item (Il envoie également le \texttt{client\_id} et le \textit{client\_secret} pour rendre compte de l'identité du client)
		\item Le serveur est capable de vérifier le ``code verifier'' et le ``code challenge'' sur la base des données reçues dans les deux requêtes séparées
		\item Si erreur, le serveur répond par \texttt{invalid\_grant}
		\item Cette extension n'ajoute pas de nouvelles req/rep. Du coup, même si le serveur d'autorisation ne supporte pas PKCE, les clients peuvent toujours l'utiliser
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Illustration du AC Flow with PKCE}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=-20pt]
		at (current page.center) {
			\includegraphics[width=9cm]{img/oauth2_auth_code_flow_p1.png}
		};
	\end{tikzpicture}
\end{frame}

\begin{frame}
	\frametitle{Illustration du AC Flow with PKCE -- suite}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=-20pt]
		at (current page.center) {
			\includegraphics[width=8cm]{img/oauth2_auth_code_flow_p2.png}
		};
	\end{tikzpicture}
\end{frame}

\begin{frame}
	\frametitle{Autres considérations}
	\begin{itemize}
		\item Le paramère \texttt{state} qui est envoyé et retourné par l'AS sert à deux choses : 
		\begin{enumerate}
			\item le client peut l'utiliser comme clé de session pour savoir où rediriger l'utlisateur après l'autorisation
			\item une protection CSRF s'il contient une valeur aléatoire par requête. Quand l'utilisateur est redirigé vers l'app du client, vérifier que c'est toujours la même valeur qu'initialement
		\end{enumerate}
		
		\item OAuth2 ne précise pas comment le jeton d'accès doit être vérifié par le serveur de ressources
		\item Elle indique juste qu'il faut qu'il se coordonne avec le serveur d'autorisation
		\item Dans beaucoup de cas, les deux serveurs font partie d'un même système (peuvent partager le token)
		\item Sinon, il faudra une API (token introspection) qui doit être protégée (privée ou publique mais avec authN)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Autres considérations -- suite}
	\begin{itemize}
		\item Comment les clients enregistrent leurs apps pour obtenir un \texttt{client\_id} et \texttt{client\_secret} ? via une page Web à prévoir ou bien de façon programmatique en implémentant la spec \textit{Dynamic Client Registration}
		\item Cette inscription doit permettre aux clients d'indiquer si leur app est confidentielle ou publique et préciser leur \texttt{redirect\_uris}
		\item L'AS doit pourvoir accepter l'authentification des clients (dans l'AC flow) :
		\begin{itemize}
			\item HTTP Basic Auth : \texttt{client\_id} comme username et \texttt{client\_secret} comme password
			\item ou bien via des requêtes POST avec les deux informations communiquées dans le body
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Autres considérations -- documentation}
\begin{itemize}
		\item Documenter les deux endpoints (leurs entrées et sorties)
		\item Indiquer aux dev des clients la taille des codes/tokens générés 
		\item Leur indiquer les restrictions sur les \texttt{redirect\_uris}, comme la non-acceptation des URLs non-TLS
		\item Leur préciser s'il y a des scopes par défaut, si jamais ils ne sont pas indiqués dans la requête 1 d'autorisation
		\item Leur dire quelle est la durée de vie d'un code d'autorisation et s'il est utilisé une fois seulement
		\item Documenter les informations sur les jetons d'accès et d'actualisation
		\begin{itemize}
			\item[-] ça peut être utile de générer un nouveau jeton d'actualisation à chaque réception de requête d'actualisation du jeton d'accès (ça permet de faire de la rotation de jetons d'actualisation)
		\end{itemize}
	\end{itemize}
\end{frame}

\section{Protocole d'authentification OpenID Connect}

\begin{frame}
	\frametitle{OIDC : le protocole d'authentification}
	
	\begin{itemize}
		\item OAuth 2 est un framework d'autorisation qui spécifie comment on doit distribuer des jetons d'accès
		
		\item OpenID Connect est un protocole construit au dessus de OAuth2
		
		\item Il spécifie comment on doit distribuer des jetons d'identité 
		
		\item La RFC 6749 inclut la définition d'une API Web nommée ``authorization endpoint''. Cette API requiert un paramètre de requête obligatoire nommé \texttt{response\_type}
		
		\item Dans cette RFC, les valeurs possibbles sont \texttt{code} (flow AC) ou \texttt{token} (flow Implicit Grant)
		
		\item OpenID Conntect a ajouté \texttt{id\_token} comme valeur possible en combinaison avec les autres
	
\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{OIDC au dessus d'OAuth 2}
	
	\begin{itemize}	
		
		\item Dans une requête de jeton d'identité, le scope doit inclure \texttt{openid} (si ce n'est pas le cas, c'est l'un des flows OAuth2 qui va s'exécuter) 
		
		\item Différentes combinaisons sont donc possibles selon les \texttt{response\_type} et \texttt{scope}
		
		\item Toutes ne sont pas fournies par les implémentation du protocole
		
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Flux avec \textit{scope} incluant \textit{openid}}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=-8pt]
		at (current page.center) {
			\includegraphics[width=9cm]{img/oidc.png}
		};
	\end{tikzpicture}
	\begin{itemize}
		\item[] \vspace{5cm}
		\item Source : \href{https://darutk.medium.com/diagrams-of-all-the-openid-connect-flows-6968e3990660}{Medium - Takahiko Kawasaki}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Format des jetons d'identité}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=10pt]
		at (current page.center) {
			\includegraphics[width=8.5cm]{img/id_token.png}
		};
	\end{tikzpicture}
	\begin{itemize}
		\item[]\vspace{4.5cm}
		\item Liste complète des claims :\\ {\scriptsize \url{https://openid.net/specs/openid-connect-core-1_0.html\#StandardClaims}}
	\end{itemize}
\end{frame}

\section{Keycloak: un fournisseur OpenId}

\begin{frame}
	\frametitle{Qu'est-ce que Keycloak ?}
	\begin{itemize}
		\item OP: OpenId Provider ou IDP : Identity Provider
		\item Serveur Java (Angular \& JAX RS Wildfly) d'authN \& authZ
		\item Projet Apache géré et maintenu par Red Hat
		\item Version gratuite Community (celle qu'on va utiliser) \& version commerciale \textbf{Red Hat SSO}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Features de Keycloak}
	\begin{itemize}
		\item Single Sign-on \& Single Sign-off
		\item Implémente les protocoles standards OAuth 2 et OIDC (mais aussi SAML)
		\item Possibilité d'utiliser les mécanismes built-in (interfaces Web personnalisables via des thèmes) ou bien d'interagir avec Keycloak via des bibliothèques Java, JavaScript, Go, ... ou bien encore via HTTP
		\item Support simplifié pour la multi-factor authN, via des app mobiles entre autres
		\item Login via des réseaux sociaux : Google, FB, Github, ... (utilisés comme \textit{identity providers})
		\item Possible intégration de répertoires d'utilisateurs LDAP, comme Active Directory, ...
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Concepts de base dans Keycloak}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=-20pt]
		at (current page.center) {
			\includegraphics[width=10cm]{img/keycloak_concepts.png}
		};
	\end{tikzpicture}
	
\end{frame}

\begin{frame}
	\frametitle{Installation de Keycloak}
	\begin{itemize}
		\item Récupérer le code dans le dépôt Git (dossier code/)
		\item C'est une orchestration de services Docker : 
		\begin{enumerate}
			\item serveur Keyclock, 
			\item sa base de données Postgres,
			\item et une API Spring Boot avec quelques endpoints sécurisés
		\end{enumerate}
				
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Exemple illustratif - les services Docker}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=-15pt]
		at (current page.center) {
			\includegraphics[width=9cm]{img/compose1.png}
		};
	\end{tikzpicture}	
\end{frame}

\begin{frame}
	\frametitle{Exemple illustratif - les services Docker -suite-}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=-20pt]
		at (current page.center) {
			\includegraphics[width=10cm]{img/compose2.png}
		};
	\end{tikzpicture}	
\end{frame}

\begin{frame}
	\frametitle{Exemple illustratif - les services Docker}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=-15pt]
		at (current page.center) {
			\includegraphics[width=10cm]{img/compose3.png}
		};
	\end{tikzpicture}	
\end{frame}

\begin{frame}
	\frametitle{Exemple illustratif - les Dockerfile}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=50pt]
		at (current page.center) {
			\includegraphics[width=8cm]{img/dockerfile_keycloak.png}
		};
	\end{tikzpicture}	
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=-36pt,yshift=0pt]
		at (current page.center) {
			\includegraphics[width=5.5cm]{img/dockerfile_postgres.png}
		};
	\end{tikzpicture}
	
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=-70pt]
		at (current page.center) {
			\includegraphics[width=8cm]{img/dockerfile_api.png}
		};
	\end{tikzpicture}
\end{frame}


\begin{frame}
	\frametitle{Exemple illustratif - les .env}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=-40pt,yshift=20pt]
		at (current page.center) {
			\includegraphics[width=6cm]{img/dotenv_keycloak.png}
		};
	\end{tikzpicture}	
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=-40pt,yshift=-80pt]
		at (current page.center) {
			\includegraphics[width=6cm]{img/dotenv_postgres.png}
		};
	\end{tikzpicture}
	
\end{frame}

\begin{frame}
	\frametitle{Exemple illustratif - le .env du MS user}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=20pt]
		at (current page.center) {
			\includegraphics[width=10cm]{img/dotenv_api.png}
		};
	\end{tikzpicture}	
	
\end{frame}


\begin{frame}
	\frametitle{Configuration de la sécurité Spring}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=-15pt]
		at (current page.center) {
			\includegraphics[width=9cm]{img/sec_config.png}
		};
	\end{tikzpicture}	
\end{frame}

\begin{frame}
	\frametitle{Configuration de Keycloak}
	\begin{itemize}
		\item Dans l'exemple, j'ai créé un realme : \texttt{demo}\footnote[frame]{Il est possible d'exporter le realm au format JSON et de l'importer au démarrage du conteneur Keycloack}
		\item J'ai créé un client OpenID Connect : \texttt{client-demo}
		\item J'ai créé un user (\texttt{admin-user}) avec un credential/password indiqué le api.env (du MS User)
		\item J'ai ajouté le rôle \texttt{manage-users} du client \texttt{realm-management}
		
		
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Sécuriser un microservice avec Keycloak}
	\begin{itemize}
		\item Voir le code du contrôleur REST et du service en aval
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Référence biblio}
	\begin{itemize}
		\item \url{https://www.youtube.com/watch?v=FyVHNJNriUQ}
	\end{itemize}
\end{frame}


\begin{frame}
	\begin{tikzpicture}[overlay,remember picture]
		\node[anchor=center,xshift=0pt,yshift=0pt]
		at (current page.center) {
			\includegraphics[width=4cm]{img/question.jpg}
		};
	\end{tikzpicture}
\end{frame}

\end{document}
