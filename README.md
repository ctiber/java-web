# Ce dépôt

Dans ce dépôt, vous aurez accès à toutes les ressources utilisées dans les cours et TP sur le développement modulaire d'applications Web avec Java, en utilisant les Servlets, Spring MVC, Spring Boot, Testing, Security, WebFlux, ...

## Semaine 1 :
Introduction au style d'architectures à microservices
- [Cours 0](./c0_intro_archi_web_ms/c0_archi_microservices.pdf)

Notions de base autour de la programmation Web avec Java
- [Cours 1](./c1_java_web/c1_java_web.pdf)
- [TP 1](./tp1_java_web/tp1_java_web.pdf)


## Semaine 2 :
Modulariser les applications Java avec Spring (Injection de dépendances)
- [Cours 2](./c2_spring_di/c2_spring_di.pdf)

Bien structurer une application Web avec Spring MVC
- [Cours 3 et TP 2](./c3_spring_mvc/c3_spring_mvc.pdf)

## Semaine 3 :
Auto-configurer une application Web avec Spring Boot
- [Cours 4 et TP 3](./c4_spring_boot/c4_spring_boot.pdf)
- [Script de création des tables de la base de données](./c4_spring_boot/code/create_tables_covid.sql)
- [Sujet de projet](./c4_spring_boot/sujet_projet.pdf)

## Semaine 4 :
Sécuriser une application Web avec Spring Security
- [Cours 5 et TP 4](./c5_ms_security/c5_ms_security.pdf)
- [login.jsp](./c5_ms_security/code/login.jsp)
- [register.jsp](./c5_ms_security/code/register.jsp)

Sécuriser une application Web avec OAuth2, OpenID Connect et Keycloak
- [Cours 5 bis](./c5bis_oauth2_oidc_keycloak/c5bis_security.pdf)

## Semaine 5 :
Tester une application Web avec Spring Testing
- [Cours 6 et TP 5](./c7_spring_testing/c7_spring_testing.pdf)
